import onnxruntime
from PIL import Image
from torchvision import transforms


input_image = Image.open("app/street_min.jpg")
preprocess = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                         std=[0.229, 0.224, 0.225]),
])
input_tensor = preprocess(input_image)
input_batch = input_tensor.unsqueeze(0)

ort_session = onnxruntime.InferenceSession("app/deeplabv3.onnx")


def to_numpy(tensor):
    return tensor.cpu().numpy()


ort_inputs = {ort_session.get_inputs()[0].name: to_numpy(input_batch)}
ort_outs = ort_session.run(None, ort_inputs)
print(ort_outs)